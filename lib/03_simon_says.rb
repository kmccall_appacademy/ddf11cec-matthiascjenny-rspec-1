def echo(string)
  return string
end

def shout(string)
  return string.upcase
end

def repeat(string,n=2)
  array = []
  n.times {array.push(string)}
  return array.join(" ")
end

def start_of_word(string,n)
  return string[0,n]
end

def first_word(string)
  array = string.split(" ")
  return array[0]
end

def titleize(string)
  array = string.split(" ")
  if array.length == 1
    return array[0].capitalize
  else
    first = array[0]
    array.shift
    array.each_with_index { |n,i|
      unless n == "a" || n == "an" || n == "the" || n == "at" || n == "by" ||
        n == "for" || n == "in" || n == "of" || n == "on" || n == "to" ||
        n == "up" || n == "and" || n == "as" || n == "but" || n == "or" ||
        n == "nor" || n == "over" then
        array[i] = n.capitalize
      end
    }
    return first.capitalize + " " + array.join(" ")
  end
end
