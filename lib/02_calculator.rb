def add(n,m)
  return n+m
end

def subtract(n,m)
  return n - m
end

def sum(array)
  result = 0
  array.each {|n| result += n}
  return result
end

def multiply(array)
  result = 1
  array.each {|n| result *= n}
  return result
end

def power(n,m)
  return n ** m
end

def factorial(n)
  if n == 0
    return 1
  elsif n == 1
    return 1
  else
    return factorial(n-1)*n
  end
end
